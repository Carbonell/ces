import htsjdk.samtools.SAMRecordIterator;
import htsjdk.samtools.SamReader;
import htsjdk.samtools.reference.IndexedFastaSequenceFile;
import java.util.List;
import java.util.Map;
//import GenomeProfiler.WindowSummaryOptions;


public class WindowProcessor implements Runnable{

    private String chr;
    private int start;
    private int end;
    private Map<String,Integer> genomeContigsLength;
    private SamReader[] bamReaders;
    private IndexedFastaSequenceFile refReader;
    private List<String> sampleNames;
    private Window window;
    private GenomeProfiler.WindowSummaryOptions windowSummary;

    public WindowProcessor(String chr, int start, int end, Map<String,Integer> genomeContigsLength, SamReader[] bamReaders, List<String> sampleNames, IndexedFastaSequenceFile refReader, GenomeProfiler.WindowSummaryOptions windowSummary) {
        this.chr = chr;
        this.start = start;
        this.end = end;
        this.genomeContigsLength = genomeContigsLength;
        this.bamReaders = bamReaders;
        this.sampleNames = sampleNames;
        this.refReader = refReader;
        this.windowSummary = windowSummary;
    }

    public void run() {

        // check coordinates consistency
        if(start<1) {
            System.err.println("Warning: start= " + start);
            start = 1;
        }
        if(end>this.genomeContigsLength.get(chr)) {
            System.err.println("Warning: end (" + end + ") > contig length (" + this.genomeContigsLength.get(chr) + ")");
            end = this.genomeContigsLength.get(chr);
        }

        // Samples pileup
        Batch[] batches = new Batch[this.bamReaders.length];
        for(int i=0; i<this.bamReaders.length; i++) {

            synchronized(this.refReader) {
                batches[i] = new Batch(this.sampleNames.get(i), chr, start, end, this.refReader.getSubsequenceAt(chr, start, end).getBases());
            }
            synchronized(this.bamReaders) {
                SAMRecordIterator it = this.bamReaders[i].query(chr, start, end, false);
                while (it.hasNext()) {
                    batches[i].processRead(it.next());
                }
                it.close();
            }
            batches[i].summarize();
        }

        // process window
        try {
            this.window = new Window(this.chr,this.start,this.end,this.windowSummary);
            this.window.processWindow(batches);
        } catch (NoSuchFieldException e){
            e.printStackTrace();
        } catch (IllegalAccessException e){
            e.printStackTrace();
        }


    }


    public Window getWindow() {
        return window;
    }

    public void setWindow(Window window) {
        this.window = window;
    }
}
