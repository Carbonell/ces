import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class ParameterOverview {

    private boolean initialized;
    private List<String> parameterNames;
    private Map<String, Integer> parameterIndex;
    private int numberOfParameters;
    private double[] minValues;
    private double[] maxValues;

    public ParameterOverview(){
        this.initialized = false;
        this.parameterIndex = new HashMap<>();
    }

    public void processWindow(Window window){
        if(!initialized) initParameterList(window);
        for(String name:window.getParameterNames()) processParameter(name,window.getParameterList().get(name));
    }

    private void processParameter(String name, double value){
        int index = parameterIndex.get(name);
        //System.err.println(name + " " + value + " [" + minValues[index] + "," + maxValues[index] + "]");
        if(value<minValues[index]) minValues[index] = value;
        if(value>maxValues[index]) maxValues[index] = value;
    }


//    private void initParameterList(){
//        parameterNames = new ArrayList<>();
//    }

    private void initParameterList(Window window){

        parameterNames = window.getParameterNames();
        numberOfParameters = parameterNames.size();
        for(int i=0; i<numberOfParameters; i++) parameterIndex.put(parameterNames.get(i),i);

        minValues = new double[parameterNames.size()];
        Arrays.fill(minValues,Double.MAX_VALUE);

        maxValues = new double[parameterNames.size()];
        Arrays.fill(maxValues,Double.MIN_VALUE);

        this.initialized = true;
    }

    public String toString(){
        StringBuffer out = new StringBuffer();
        out.append("parameter\tmin\tmax");
        for(int i=0; i<numberOfParameters; i++){
            out.append("\n").append(parameterNames.get(i)).append("\t").append(minValues[i]).append("\t").append(maxValues[i]);
        }
        return(out.toString());
    }

    public List<String> getParameterNames() {
        return parameterNames;
    }

//    public static ParameterOverview loadParameterStats(String parametersStatsFile) throws IOException {
//
//        ParameterOverview po = new ParameterOverview();
//        BufferedReader br = new BufferedReader(new FileReader(parametersStatsFile));
//        br.readLine();
//        String line;
//        while((line=br.readLine())!=null){
//            String values[] = line.split("\t");
//        }
//        br.close();
//
//    }

    public Map<String, Integer> getParameterIndex() {
        return parameterIndex;
    }

    public int getNumberOfParameters() {
        return numberOfParameters;
    }

    public double[] getMinValues() {
        return minValues;
    }

    public double[] getMaxValues() {
        return maxValues;
    }
}
