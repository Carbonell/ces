import java.awt.geom.QuadCurve2D;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Histogram {

    private String name;
    private String histogramFile;

    private double min;
    private double max;
    private int bins;
    private double step;

    private double mins[];
    private double maxs[];
    private int counts[];

    private int[] cumulativeCounts;
    private double[] quantiles;


    public Histogram(String name, String histogramFile) throws IOException {
        this.name = name;
        this.histogramFile = histogramFile;
        loadHistogram();
    }

    public Histogram(String name, double min, double max, int bins){
        this.name = name;
        this.min = min;
        this.max = max;
        this.bins = bins;
        createIntervals();
    }

    private void createIntervals(){

        this.step = (this.max - this.min) / (double) this.bins;

        mins = new double[this.bins];
        maxs = new double[this.bins];
        counts = new int[this.bins];

        double current = this.min;
        for(int i=0; i<this.bins; i++){
            mins[i] = current;
            maxs[i] = current + this.step;
            current += this.step;
            counts[i] = 0;
        }


    }

    public void addValue(double value) {
        int index = getIndex(value);
        counts[index]++;
    }

    public void loadHistogram() throws IOException {

        // load histogram
        List<Double> minsList = new ArrayList<>();
        List<Double> maxsList = new ArrayList<>();
        List<Integer> countsList = new ArrayList<>();
        List<Integer> cumulativeCountsList = new ArrayList<>();
        List<Double> quantilesList = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(histogramFile));
        br.readLine();
        String line;
        while ((line = br.readLine()) != null) {
            String values[] = line.split("\t");
            minsList.add(Double.parseDouble(values[0]));
            maxsList.add(Double.parseDouble(values[1]));
            countsList.add(Integer.parseInt(values[2]));
            cumulativeCountsList.add(Integer.parseInt(values[3]));
            quantilesList.add(Double.parseDouble(values[4]));
        }
        br.close();

        // set class vectors
        this.bins = minsList.size();
        this.mins = new double[this.bins];
        this.maxs = new double[this.bins];
        this.counts = new int[this.bins];
        this.cumulativeCounts = new int[this.bins];
        this.quantiles = new double[this.bins];
        for(int i=0; i<minsList.size(); i++){
            this.mins[i] = minsList.get(i);
            this.maxs[i] = maxsList.get(i);
            this.counts[i] = countsList.get(i);
            this.cumulativeCounts[i] = cumulativeCountsList.get(i);
            this.quantiles[i] = quantilesList.get(i);
        }
        this.min = this.mins[0];
        this.max = this.maxs[bins-1];
        this.step = (this.max - this.min) / (double) this.bins;

    }

    public void computeQuantiles(){

        this.cumulativeCounts = new int[this.bins];
        int sum = 0;
        for(int i=0; i<this.bins; i++){
            sum += this.counts[i];
            this.cumulativeCounts[i] = sum;
        }

        this.quantiles = new double[this.bins];
        for(int i=0; i<this.bins; i++){
            this.quantiles[i] = (double)this.cumulativeCounts[i]/(double)sum;
        }

    }

    public double estimateQuantile(double value){

        if(new Double(value).isNaN()) {
            return(new Double(0).NaN);
        } else {
            double quantile;
            if (value < this.min) {
                //System.err.println("WARNING: value " + value + " is out of range [" + this.min + "," + this.max + "] in param " + this.name);
                quantile = 0;
            } else if (value > this.max) {
                //System.err.println("WARNING: value " + value + " is out of range [" + this.min + "," + this.max + "] in param " + this.name);
                quantile = 1;
            } else {

                int index = getIndex(value);
                double leftQuantile, rightQuantile;

                if (index == 0) leftQuantile = 0.0;
                else leftQuantile = quantiles[index - 1];
                rightQuantile = quantiles[index];

                double w = (value - mins[index]) / (maxs[index] - mins[index]);

                quantile = leftQuantile * (1.0 - w) + rightQuantile * w;

            }

            return (quantile);
        }
    }

    public String toString(){

        StringBuffer out = new StringBuffer();
        out.append("min\tmax\tcounts\tcumulativeCounts\tquantiles");
        for(int i=0; i<this.bins; i++){
            out.append("\n").
                    append(mins[i]).append("\t").
                    append(maxs[i]).append("\t").
                    append(counts[i]).append("\t").
                    append(cumulativeCounts[i]).append("\t").
                    append(quantiles[i]);
        }
        out.append("\n");
        return(out.toString());
    }

    private int getIndex(double value){

        int index;
        if (value <= this.min) {
            index = 0;
        } else if (value >= this.max) {
            index = this.bins-1;
        } else {
            index = (int) Math.floor((value - this.min)/step);
        }
        return(index);

    }

    public String getName() {
        return name;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public int getBins() {
        return bins;
    }

    public double[] getMins() {
        return mins;
    }

    public double[] getMaxs() {
        return maxs;
    }

    public int[] getCounts() {
        return counts;
    }

}
