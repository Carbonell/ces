/**
 * Created by jose on 3/28/16.
 */
public class Region {

    private String chr;
    private int start;
    private int end;

    public Region(String chr, int start, int end){
        this.chr = chr;
        this.start = start;
        this.end = end;
    }

    public Region(String region) {
        String[] first = region.split(":");
        String[] second = first[1].split("-");
        this.chr = first[0];
        this.start = Integer.parseInt(second[0]);
        this.end = Integer.parseInt(second[1]);
    }

    public String toString(){
        return chr + ":" + start + "-" + end;
    }

    public String getChr(){
        return this.chr;
    }

    public int getStart(){
        return this.start;
    }

    public int getEnd(){
        return this.end;
    }

}
