
import org.apache.commons.cli.*;
import java.io.IOException;
import java.io.File;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class GenomeProfiling {


    public static void main(String [] args) throws Exception {


//        List<Double> qs = new ArrayList<Double>();
//        qs.add(0.0);
//        qs.add(0.0);
//        qs.add(0.0);
//        qs.add(0.0);
//        CESComputer cc = new CESComputer();
//        System.err.println(qs);
//        cc.computeCES(qs);
//        System.err.println(cc.toString());
//
//        List<Double> qs2 = new ArrayList<Double>();
//        qs2.add(0.49);
//        qs2.add(0.81);
//        qs2.add(0.10);
//        qs2.add(0.055);
//        System.err.println(qs2);
//        cc.computeCES(qs2);
//        System.err.println(cc.toString());

        // init options
        Options options = new Options();

        /////// BUILD mode options
        options.addOption("build", false, "Build profile");

        // ref
        options.addOption("r", true, "Fasta reference");
        //options.getOption("r").setRequired(true);

        // bams
        options.addOption("b", true, "BAM list file");

        // output folder
        options.addOption("o", true, "Output profile folder");

        // regions
        options.addOption("region", true, "Region");
        options.addOption("bed", true, "Bed file");

        // windowing
        options.addOption("ws", true, "Window size");
        options.addOption("bins", true, "Number of empirical distribution bins");
        options.addOption("t", true, "Number of threads");

        /////// TEST mode options
        options.addOption("test", false, "Test coordinates ");
        options.addOption("target", true, "Target coordinates file (bed format)");
        options.addOption("p", true, "Input profile folder");
        options.addOption("s", true, "Window summary [mean,linear]");
        //options.addOption("o", true, "Output folder");

        // parse cli
        CommandLine cmd = null;

        try {

            cmd = new DefaultParser().parse(options, args);

            if(cmd.hasOption("build")) {

                String refFile = cmd.getOptionValue("r");
                String bamFiles = cmd.getOptionValue("b");
                String outputFolder = cmd.getOptionValue("o");
                String region = cmd.getOptionValue("region");
                String bedFile = cmd.getOptionValue("bed");

                // check parameters
                if(refFile==null){
                    throw new ParseException("ERROR: reference fasta must be provided");
                }
                if(bamFiles==null){
                    throw new ParseException("ERROR: bam files must be provided");
                }
                if(outputFolder==null){
                    throw new ParseException("ERROR: output folder must be provided");
                }

                // init caller
                GenomeProfiler gp = new GenomeProfiler(bamFiles,refFile,outputFolder);
                if(bedFile!=null) gp.setRegionFile(bedFile);
                if(region!=null) gp.setRegion(region);

                if(cmd.hasOption("ws")){
                    int windowSize = Integer.parseInt(cmd.getOptionValue("ws"));
                    gp.setWindowSize(windowSize);
                }
                if(cmd.hasOption("bins")){
                    int bins = Integer.parseInt(cmd.getOptionValue("bins"));
                    gp.setBins(bins);
                }
                if(cmd.hasOption("t")){
                    int cores = Integer.parseInt(cmd.getOptionValue("t"));
                    gp.setCores(cores);
                }
                if(cmd.hasOption("s")){
                    String windowSummary = cmd.getOptionValue("s");
                    gp.setWindowSummary(windowSummary);
                }

                // run
                if(!new File(outputFolder).exists()) new File(outputFolder).mkdir();
                PrintWriter cli = new PrintWriter(new File(outputFolder + "/cli_build.txt"));
                cli.write(String.join(" ",args));
                cli.close();
                gp.run();


            } else {

                if(cmd.hasOption("test")) {

                    String targetFile = cmd.getOptionValue("target");
                    String profileFolder = cmd.getOptionValue("p");
                    String outputFolder = cmd.getOptionValue("o");

                    if (targetFile == null) {
                        System.err.println("ERROR: target file must be provided");
                        printUsage();
                    }
                    if (profileFolder == null) {
                        System.err.println("ERROR: profile folder must be provided");
                        printUsage();
                    }
                    if (outputFolder == null) {
                        System.err.println("ERROR: output folder must be provided");
                        printUsage();
                    }

                    GenomeTester gt = new GenomeTester(targetFile,profileFolder,outputFolder);
                    if(!new File(outputFolder).exists()) new File(outputFolder).mkdir();
                    PrintWriter cli = new PrintWriter(new File(outputFolder + "/cli_test.txt"));
                    cli.write(String.join(" ",args));
                    cli.close();
                    gt.test();

                } else {
                    System.err.println("ERROR: mode build/test must be specify");
                    printUsage();
                }

            }


        } catch(ParseException pe){
            System.err.println(pe.getMessage());
            printUsage();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void printUsage(){

        StringBuffer out = new StringBuffer();
        out.append("\n");
        out.append(" CES (combined error score)\n");
        out.append(" ||||||||||||||||||||||||||\n");
        out.append("\n");
        out.append("\n");
        out.append("BUILD MODE:\n");
        out.append("\n");
        out.append("    -build -r <ref.fasta> -ws <window length> -b <samples.list> -o <output_folder> [-region chr:start-end] [-bed regions.bed] [-bins number_of_bins]\n");
        out.append("\n");
        out.append("TEST MODE\n");
        out.append("\n");
        out.append("    -test -target <design.txt> -p <profile_folder> -o <output_folder>\n");
        out.append("\n");
        out.append("PARAMETERS\n");
        out.append("    -r         Reference genome under evaluation (FASTA format)\n");
        out.append("    -ws        Window length\n");
        out.append("    -b         Mapped samples to perform de evaluation (BAM format)\n");
        out.append("    -o         Output folder\n");
        out.append("    -region    Constrain the CES computing to a specific region (chr:start-end)\n");
        out.append("    -bed       Constrain the CES computing to a specific list of regions (BED format)\n");
        out.append("    -bins      Number of bins used to construct parameter histograms (default 1000)\n");
        out.append("    -target    Regions to test (BED format)\n");
        out.append("    -p         Profile to used in testing (output folder used in build)\n");

        System.out.println(out.toString());
    }

}
