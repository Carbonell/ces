
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.math3.distribution.ChiSquaredDistribution;

public class ResultsTable {

    private List<String> chr;
    private List<Integer> start;
    private List<Integer> end;
    private List<String> parameterNames;
    private HashMap<String,List<Double>> columns;
    private List<String> chrWindow;
    private List<Integer> startWindow;
    private List<Integer> endWindow;
    private int numberOfRows;

    public ResultsTable(List<String> parameterNames){
        this.parameterNames = parameterNames;
        this.columns = new HashMap<>();
        for(String name:parameterNames){
            columns.put(name,new ArrayList<Double>());
        }
        columns.put("ces",new ArrayList<Double>());
        chr = new ArrayList<>();
        start = new ArrayList<>();
        end = new ArrayList<>();
        chrWindow = new ArrayList<>();
        startWindow = new ArrayList<>();
        endWindow = new ArrayList<>();
        this.numberOfRows = 0;
    }

    public void addRow(String chr, int start, int end, String[] fields, String[] values){

        // query coords
        this.chr.add(chr);
        this.start.add(start);
        this.end.add(end);

        // target coords
        this.chrWindow.add(values[0]);
        this.startWindow.add(Integer.parseInt(values[1]));
        this.endWindow.add(Integer.parseInt(values[2]));

        for(int i=3; i<values.length; i++){
            if(columns.containsKey(fields[i])) columns.get(fields[i]).add(Double.parseDouble(values[i]));
        }

        this.numberOfRows++;

    }

    public void addRow(String chr, int start, int end){

        // query coords
        this.chr.add(chr);
        this.start.add(start);
        this.end.add(end);

        // target coords
        this.chrWindow.add(null);
        this.startWindow.add((int)Double.NaN);
        this.endWindow.add((int)Double.NaN);

        for(String key:columns.keySet()){
            columns.get(key).add(Double.NaN);
        }

        this.numberOfRows++;

    }

    public ResultsTable createQuantileResults(List<String> parameterNames, HashMap<String,Histogram> histograms){

        ResultsTable resultsQuantile = new ResultsTable(parameterNames);
        for(int i=0; i<this.getNumberOfRows(); i++){
            // query coords
            resultsQuantile.getChr().add(this.getChr().get(i));
            resultsQuantile.getStart().add(this.getStart().get(i));
            resultsQuantile.getEnd().add(this.getEnd().get(i));
            // target coords
            resultsQuantile.getChrWindow().add(this.getChrWindow().get(i));
            resultsQuantile.getStartWindow().add(this.getStartWindow().get(i));
            resultsQuantile.getEndWindow().add(this.getEndWindow().get(i));
            // params
            double q;
            List<Double> qs = new ArrayList<Double>();
            for(String name: parameterNames){
                if(this.getColumns().get(name).get(i)==null){
                    resultsQuantile.getColumns().get(name).add(null);
                } else {
                    q = histograms.get(name).estimateQuantile(this.getColumns().get(name).get(i));
                    resultsQuantile.getColumns().get(name).add(q);
                    if(!Double.isNaN(q) & !Double.isInfinite(q)) qs.add(q);
                }
            }
            if(qs.size()>0) {
                CESComputer cc = new CESComputer();
                cc.computeCES(qs);
                resultsQuantile.getColumns().get("ces").add(cc.getScore());
            } else {
                resultsQuantile.getColumns().get("ces").add(0.0);
            }
        }
        resultsQuantile.setNumberOfRows(this.getNumberOfRows());

        return(resultsQuantile);
    }

    public String toString(){
        StringBuffer out = new StringBuffer();
        out.append("chr\tstart\tend\tchr.window\tstart.window\tend.window");
        for(String name:parameterNames) out.append("\t").append(name);
        out.append("\n");
        for(int i=0; i<chr.size(); i++){
            out.append(chr.get(i)).append("\t");
            out.append(start.get(i)).append("\t");
            out.append(end.get(i)).append("\t");
            out.append(chrWindow.get(i)).append("\t");
            out.append(startWindow.get(i)).append("\t");
            out.append(endWindow.get(i));
            for(String name:parameterNames){
                out.append("\t").append(columns.get(name).get(i));
            }
            out.append("\n");
        }
        return(out.toString());
    }

    public String toCES(){
        StringBuffer out = new StringBuffer();
        out.append("chr\tstart\tend\tces");
        out.append("\n");
        for(int i=0; i<chr.size(); i++){
            out.append(chr.get(i)).append("\t");
            out.append(start.get(i)).append("\t");
            out.append(end.get(i)).append("\t");
            out.append(columns.get("ces").get(i));
            out.append("\n");
        }
        return(out.toString());
    }

    public List<String> getChr() {
        return chr;
    }

    public void setChr(List<String> chr) {
        this.chr = chr;
    }

    public List<Integer> getStart() {
        return start;
    }

    public void setStart(List<Integer> start) {
        this.start = start;
    }

    public List<Integer> getEnd() {
        return end;
    }

    public void setEnd(List<Integer> end) {
        this.end = end;
    }

    public List<String> getParameterNames() {
        return parameterNames;
    }

    public void setParameterNames(List<String> parameterNames) {
        this.parameterNames = parameterNames;
    }

    public HashMap<String, List<Double>> getColumns() {
        return columns;
    }

    public void setColumns(HashMap<String, List<Double>> columns) {
        this.columns = columns;
    }

    public List<String> getChrWindow() {
        return chrWindow;
    }

    public void setChrWindow(List<String> chrWindow) {
        this.chrWindow = chrWindow;
    }

    public List<Integer> getStartWindow() {
        return startWindow;
    }

    public void setStartWindow(List<Integer> startWindow) {
        this.startWindow = startWindow;
    }

    public List<Integer> getEndWindow() {
        return endWindow;
    }

    public void setEndWindow(List<Integer> endWindow) {
        this.endWindow = endWindow;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public void setNumberOfRows(int numberOfRows) {
        this.numberOfRows = numberOfRows;
    }
}
