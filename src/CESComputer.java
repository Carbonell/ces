import org.apache.commons.math3.distribution.ChiSquaredDistribution;

import java.util.List;

public class CESComputer {

    double pValue;
    double logPValue;
    double score;
    int df;
    double chisq;

    public void computeCES(List<Double> qs){

        this.df = 2*qs.size();

        double sum = 0;
        double p;
        for(Double q: qs){
            p = 1-q;
            if(p==0) p = 0.001;
            sum += Math.log(p);
        }
        this.chisq = -2 * sum;


        ChiSquaredDistribution ch2 = new ChiSquaredDistribution(df);

        this.score = ch2.cumulativeProbability(this.chisq);
        this.pValue = 1-this.score;
        if(this.pValue==0) this.logPValue = Math.log(Double.MIN_VALUE);
        else this.logPValue = Math.log(this.pValue);

    }

    public String toString(){
        return "CES ======\n chisq = " + this.chisq + "\n df = " + this.df + "\n p.value = " + pValue; // + "\n log_p.value = " + logPValue + "\n score = " + score;
    }

    public double getpValue() {
        return pValue;
    }

    public double getLogPValue() {
        return logPValue;
    }

    public double getScore() {
        return score;
    }
}
