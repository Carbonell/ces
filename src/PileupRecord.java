
import java.util.*;
import java.lang.String;

import com.sun.org.glassfish.gmbal.ParameterNames;
import htsjdk.tribble.readers.TabixReader;
import org.apache.commons.math3.stat.inference.MannWhitneyUTest;
import org.apache.commons.math3.stat.inference.ChiSquareTest;
import org.apache.commons.math3.stat.inference.TTest;
import org.apache.commons.math3.exception.NumberIsTooSmallException;

public class PileupRecord {

    private String sep = "\t";

    private String sampleName = "";

    private String chr;
    private int start;
    private int end;
    private char refAllele;
    private char altAllele;
    private String otherAltAlleles;
    private Map<Character,Integer> allelesCount;
    private boolean isReference;
    private boolean isHeterozygous;
    private boolean isHomozygous;
    private boolean isVariant;
    private boolean isAlternativeHomozygous;
    private float allelicFrequency;

    private List<Character> bases;
    private List<Boolean> strands;
    private List<Byte> baseQualities;
    private List<Byte> mappingQualities;
    private List<Integer> readPositions;
    private List<Integer> numberOfCigarElements;
    private List<Boolean> isClipped;
    private List<Boolean> hasIndels;
    private List<Boolean> isDuplicated;
    private List<Boolean> isNotPrimaryAlignment;
    private List<Boolean> isProperlyPaired;
    private List<Boolean> isMateUnmapped;

    private int dp;

    private int refAlleleCount;
    private int altAlleleCount;
    private int otherAltAlleleCount;

    private float refAlleleFrequency;
    private float altAlleleFrequency;
    private float otherAltAlleleFrequency;

    private int mappingQualityZeroCount;
    private int mappingQualityZeroCountAlt;
    private int mappingQualityZeroCountRef;
    private float mappingQualityZeroFrequency;
    private float mappingQualityZeroFrequencyAlt;
    private float mappingQualityZeroFrequencyRef;
    private int baseQualityZeroCount;
    private int baseQualityZeroCountAlt;
    private int baseQualityZeroCountRef;
    private float baseQualityZeroFrequency;
    private float baseQualityZeroFrequencyAlt;
    private float baseQualityZeroFrequencyRef;

    private int mappingQualityTenCount;
    private int mappingQualityTenCountAlt;
    private int mappingQualityTenCountRef;
    private float mappingQualityTenFrequency;
    private float mappingQualityTenFrequencyAlt;
    private float mappingQualityTenFrequencyRef;
    private int baseQualityTenCount;
    private int baseQualityTenCountAlt;
    private int baseQualityTenCountRef;
    private float baseQualityTenFrequency;
    private float baseQualityTenFrequencyAlt;
    private float baseQualityTenFrequencyRef;

    private double meanBaseQualitiesErrorProbability;
    private double meanBaseQualitiesErrorProbabilityAlt;
    private double meanBaseQualitiesErrorProbabilityRef;
    private double meanMappingQualitiesErrorProbability;
    private double meanMappingQualitiesErrorProbabilityAlt;
    private double meanMappingQualitiesErrorProbabilityRef;

    private double baseQualityRankSumZ;
    private double mappingQualityRankSumZ;
    private double readPositionRankSumZ;
    private double numberOfCigarElementsRankSumZ;

    private double baseQualityTTest;
    private double mappingQualityTTest;
    private double readPositionTTest;
    private double numberOfCigarElementsTTest;

    private double strandBiasSOR;
    private double clippingBiasSOR;
    private double indelsBiasSOR;

    private double altAlellesOdds;
    private double altAlellesFreqDiff;

    private int indelsCount;
    private int clippedCount;
    private int duplicatedCount;
    private int notPrimaryAlignmentCount;
    private int properlyPairedCount;
    private int mateUnmappedCount;

    private float indelsFrequency;
    private float clippedFrequency;
    private float duplicatedFrequency;
    private float notPrimaryAlignmentFrequency;
    private float properlyPairedFrequency;
    private float mateUnmappedFrequency;


    public PileupRecord(String sampleName, String chr, int start, int end, char refAlle){

        this.sampleName = sampleName;

        this.chr = chr;
        this.start = start;
        this.end = end;
        this.refAllele = refAlle;

        this.bases = new ArrayList();
        this.strands = new ArrayList();
        this.baseQualities = new ArrayList();
        this.mappingQualities = new ArrayList();
        this.readPositions = new ArrayList();
        this.isClipped = new ArrayList();
        this.hasIndels = new ArrayList();
        this.numberOfCigarElements = new ArrayList();
        this.isDuplicated = new ArrayList();
        this.isNotPrimaryAlignment = new ArrayList();
        this.isProperlyPaired = new ArrayList();
        this.isMateUnmapped = new ArrayList();

        this.dp = 0;
    }


    public void addReadBaseInfo(char base, boolean strand, byte baseQuality, byte mappingQuality, int readPosition, boolean isClipped, boolean hasIndels, int numberOfCigarElements, boolean isDuplicated, boolean isNotPrimaryAlignment, boolean isProperlyPaired, boolean isMateUnmapped){
        this.bases.add(base);
        this.strands.add(strand);
        this.baseQualities.add(baseQuality);
        this.mappingQualities.add(mappingQuality);
        this.readPositions.add(readPosition);
        this.isClipped.add(isClipped);
        this.hasIndels.add(hasIndels);
        this.numberOfCigarElements.add(numberOfCigarElements);
        this.isDuplicated.add(isDuplicated);
        this.isNotPrimaryAlignment.add(isNotPrimaryAlignment);
        this.isProperlyPaired.add(isProperlyPaired);
        this.isMateUnmapped.add(isMateUnmapped);
        this.dp++;
    }


    public void summarize() {
        computeCounts();
        computeStats();
    }


    public void computeCounts(){

        this.baseQualityZeroCount = 0;
        this.baseQualityZeroCountRef = 0;
        this.baseQualityZeroCountAlt = 0;
        this.baseQualityZeroFrequency = 0;
        this.baseQualityZeroFrequencyRef = 0;
        this.baseQualityZeroFrequencyAlt = 0;
        this.mappingQualityZeroCount = 0;
        this.mappingQualityZeroCountRef = 0;
        this.mappingQualityZeroCountAlt = 0;
        this.mappingQualityZeroFrequency = 0;
        this.mappingQualityZeroFrequencyRef = 0;
        this.mappingQualityZeroFrequencyAlt = 0;

        this.baseQualityTenCount = 0;
        this.baseQualityTenCountRef = 0;
        this.baseQualityTenCountAlt = 0;
        this.baseQualityTenFrequency = 0;
        this.baseQualityTenFrequencyRef = 0;
        this.baseQualityTenFrequencyAlt = 0;
        this.mappingQualityTenCount = 0;
        this.mappingQualityTenCountRef = 0;
        this.mappingQualityTenCountAlt = 0;
        this.mappingQualityTenFrequency = 0;
        this.mappingQualityTenFrequencyRef = 0;
        this.mappingQualityTenFrequencyAlt = 0;

        this.indelsCount = 0;
        this.clippedCount = 0;
        this.duplicatedCount = 0;
        this.notPrimaryAlignmentCount = 0;
        this.properlyPairedCount = 0;
        this.mateUnmappedCount = 0;

        if(this.dp==0){

            this.refAlleleCount = 0;
            this.refAlleleFrequency = 0;
            this.altAllele = '.';
            this.altAlleleCount = 0;
            this.altAlleleFrequency = 0;
            this.otherAltAlleles = ".";
            this.otherAltAlleleCount = 0;
            this.otherAltAlleleFrequency = 0;

            this.allelicFrequency = 0;
            this.isReference = true;
            this.isHeterozygous = false;
            this.isHomozygous = false;
            this.isVariant = false;
            this.isAlternativeHomozygous = false;

            this.indelsFrequency = 0;
            this.clippedFrequency = 0;
            this.duplicatedFrequency = 0;
            this.notPrimaryAlignmentFrequency = 0;
            this.properlyPairedFrequency = 0;
            this.mateUnmappedFrequency = 0;

        } else {

            // get alleles count
            this.allelesCount = new HashMap<Character,Integer>();
            for(int i=0; i<dp; i++){
                if(allelesCount.containsKey(this.bases.get(i))){
                    int current = allelesCount.get(this.bases.get(i));
                    allelesCount.put(this.bases.get(i),current+1);
                } else {
                    allelesCount.put(this.bases.get(i),1);
                }
            }

            // ref allele count
            if(allelesCount.containsKey(this.refAllele)) {
                this.refAlleleCount = this.allelesCount.get(this.refAllele);
            } else {
                this.refAlleleCount = 0;
            }

            // get alt alleles
            Map<Character,Integer> altAllelesCount = new HashMap<Character,Integer>(this.allelesCount);
            if(allelesCount.containsKey(this.refAllele)) altAllelesCount.remove(this.refAllele);
            if(altAllelesCount.size()==0){
                this.altAllele = '.';
                this.altAlleleCount = 0;
                this.otherAltAlleles = ".";
                this.otherAltAlleleCount = 0;
            } else {
                int max = -1;
                char maxAllele = '.';
                for(Character key:altAllelesCount.keySet()) {
                    if (altAllelesCount.get(key) > max) {
                        max = altAllelesCount.get(key);
                        maxAllele = key;
                    }
                }

                // alt allele
                this.altAllele = maxAllele;
                this.altAlleleCount = max;

                // other alt alleles
                altAllelesCount.remove(this.altAllele);
                if(altAllelesCount.size()==0){
                    this.otherAltAlleles = ".";
                    this.otherAltAlleleCount = 0;
                } else {
                    this.otherAltAlleles = Arrays.toString(altAllelesCount.keySet().toArray());
                    this.otherAltAlleleCount = 0;
                    for(Character key:altAllelesCount.keySet()) this.otherAltAlleleCount += altAllelesCount.get(key);
                }

            }

            this.refAlleleFrequency = (float)this.refAlleleCount / (float) this.dp;
            this.altAlleleFrequency = (float)this.altAlleleCount / (float) this.dp;
            this.otherAltAlleleFrequency = (float)this.otherAltAlleleCount / (float) this.dp;

            // Genotype
            if(this.dp<4){
                this.isVariant = false;
                this.allelicFrequency = 0;
                this.isReference = true;
                this.isHeterozygous = false;
                this.isHomozygous = false;
                this.isAlternativeHomozygous = false;
            } else {
                if (this.altAlleleFrequency > 0.15) {
                    this.isVariant = true;
                    if (this.altAlleleFrequency > 0.9) {
                        this.allelicFrequency = 1;
                        this.isReference = false;
                        this.isHeterozygous = false;
                        this.isHomozygous = true;
                        this.isAlternativeHomozygous = true;
                    } else {
                        this.allelicFrequency = (float) 0.5;
                        this.isReference = false;
                        this.isHeterozygous = true;
                        this.isHomozygous = false;
                        this.isAlternativeHomozygous = false;
                    }
                } else {
                    this.isVariant = false;
                    this.allelicFrequency = 0;
                    this.isReference = true;
                    this.isHeterozygous = false;
                    this.isHomozygous = false;
                    this.isAlternativeHomozygous = false;
                }
            }

            // Ten and ten counts
            for(int i=0; i<dp; i++) {
                if(this.baseQualities.get(i)<=10) {

                    this.baseQualityTenCount++;
                    if (this.bases.get(i).equals(this.refAllele)) this.baseQualityTenCountRef++;
                    else this.baseQualityTenCountAlt++;

                    if (this.baseQualities.get(i) == 0) {
                        this.baseQualityZeroCount++;
                        if (this.bases.get(i).equals(this.refAllele)) this.baseQualityZeroCountRef++;
                        else this.baseQualityZeroCountAlt++;
                    }
                }
                if(this.mappingQualities.get(i)<=10) {
                    this.mappingQualityTenCount++;
                    if(this.bases.get(i).equals(this.refAllele)) this.mappingQualityTenCountRef++;
                    else this.mappingQualityTenCountAlt++;

                    if(this.mappingQualities.get(i)==0) {
                        this.mappingQualityZeroCount++;
                        if(this.bases.get(i).equals(this.refAllele)) this.mappingQualityZeroCountRef++;
                        else this.mappingQualityZeroCountAlt++;
                    }

                }
            }
            this.baseQualityZeroFrequency = (float)this.baseQualityZeroCount / (float) this.dp;
            this.baseQualityZeroFrequencyRef = (float)this.baseQualityZeroCountRef / (float) this.dp;
            this.baseQualityZeroFrequencyAlt = (float)this.baseQualityZeroCountAlt / (float) this.dp;
            this.mappingQualityZeroFrequency = (float)this.mappingQualityZeroCount / (float) this.dp;
            this.mappingQualityZeroFrequencyRef = (float)this.mappingQualityZeroCountRef / (float) this.dp;
            this.mappingQualityZeroFrequencyAlt = (float)this.mappingQualityZeroCountAlt / (float) this.dp;

            this.baseQualityTenFrequency = (float)this.baseQualityTenCount / (float) this.dp;
            this.baseQualityTenFrequencyRef = (float)this.baseQualityTenCountRef / (float) this.dp;
            this.baseQualityTenFrequencyAlt = (float)this.baseQualityTenCountAlt / (float) this.dp;
            this.mappingQualityTenFrequency = (float)this.mappingQualityTenCount / (float) this.dp;
            this.mappingQualityTenFrequencyRef = (float)this.mappingQualityTenCountRef / (float) this.dp;
            this.mappingQualityTenFrequencyAlt = (float)this.mappingQualityZeroCountAlt / (float) this.dp;

            // Boolean counts
            for(int i=0; i<dp; i++) {
                if(hasIndels.get(i)) this.indelsCount++;
                if(isClipped.get(i)) this.clippedCount++;
                if(isDuplicated.get(i)) this.duplicatedCount++;
                if(isNotPrimaryAlignment.get(i)) this.notPrimaryAlignmentCount++;
                if(isProperlyPaired.get(i)) this.properlyPairedCount++;
                if(isMateUnmapped.get(i)) this.mateUnmappedCount++;
            }

            this.indelsFrequency = (float)this.indelsCount / (float) this.dp;
            this.clippedFrequency = (float)this.clippedCount / (float) this.dp;
            this.duplicatedFrequency = (float)this.duplicatedCount / (float) this.dp;
            this.notPrimaryAlignmentFrequency = (float)this.notPrimaryAlignmentCount / (float) this.dp;
            this.properlyPairedFrequency = (float)this.properlyPairedCount / (float) this.dp;
            this.mateUnmappedFrequency = (float)this.mateUnmappedCount / (float) this.dp;

        }

    }

    public void computeStats(){

        if(this.dp==0){

            this.meanBaseQualitiesErrorProbability = 0;
            this.meanBaseQualitiesErrorProbabilityRef = 0;
            this.meanBaseQualitiesErrorProbabilityAlt = 0;
            this.meanMappingQualitiesErrorProbability = 0;
            this.meanMappingQualitiesErrorProbabilityRef = 0;
            this.meanMappingQualitiesErrorProbabilityAlt = 0;
            this.baseQualityRankSumZ = 0;
            this.mappingQualityRankSumZ = 0;
            this.readPositionRankSumZ = 0;
            this.strandBiasSOR = 1;
            this.altAlellesOdds = 1;
            this.altAlellesFreqDiff = 0;

        } else {

            // with all bases
            this.meanBaseQualitiesErrorProbability = computeMeanErrorProbability(this.baseQualities);
            this.meanMappingQualitiesErrorProbability = computeMeanErrorProbability(this.mappingQualities);

            // ref vs alt
            List<Integer> refIndexes = new ArrayList<>();
            List<Integer> altIndexes = new ArrayList<>();
            int refPosCount,refNegCount,altPosCount,altNegCount;
            refPosCount = refNegCount = altPosCount = altNegCount = 0;
            int refClippedCount,refUnclippedCount,altClippedCount,altUnclippedCount;
            refClippedCount = refUnclippedCount = altClippedCount = altUnclippedCount = 0;
            int refHasIndelsCount,refHasNotIndelsCount,altHasIndelsCount,altHasNotIndelsCount;
            refHasIndelsCount = refHasNotIndelsCount = altHasIndelsCount = altHasNotIndelsCount = 0;
            for(int i=0; i<this.dp; i++){
                if(this.bases.get(i)==this.refAllele){
                    refIndexes.add(i);
                    if(this.strands.get(i)) refPosCount++;
                    else refNegCount++;
                    if(this.isClipped.get(i)) refClippedCount++;
                    else refUnclippedCount++;
                    if(this.hasIndels.get(i)) refHasIndelsCount++;
                    else refHasNotIndelsCount++;
                }
                if(this.bases.get(i)==this.altAllele){
                    altIndexes.add(i);
                    if(this.strands.get(i)) altPosCount++;
                    else altNegCount++;
                    if(this.isClipped.get(i)) altClippedCount++;
                    else altUnclippedCount++;
                    if(this.hasIndels.get(i)) altHasIndelsCount++;
                    else altHasNotIndelsCount++;
                }
            }

            this.baseQualityRankSumZ = computeMannWhitneyU(this.baseQualities,refIndexes,altIndexes);
            this.mappingQualityRankSumZ = computeMannWhitneyU(this.mappingQualities,refIndexes,altIndexes);
            this.readPositionRankSumZ = computeMannWhitneyU(this.readPositions,refIndexes,altIndexes);
            this.numberOfCigarElementsRankSumZ = computeMannWhitneyU(this.numberOfCigarElements,refIndexes,altIndexes);

            this.baseQualityTTest = computeTTest(this.baseQualities,refIndexes,altIndexes);
            this.mappingQualityTTest = computeTTest(this.mappingQualities,refIndexes,altIndexes);
            this.readPositionTTest = computeTTest(this.readPositions,refIndexes,altIndexes);
            this.numberOfCigarElementsTTest = computeTTest(this.numberOfCigarElements,refIndexes,altIndexes);

            this.strandBiasSOR = computeStrandBiasSOR(refPosCount,refNegCount,altPosCount,altNegCount,true);
            this.clippingBiasSOR = computeStrandBiasSOR(refClippedCount,refUnclippedCount,altClippedCount,altUnclippedCount,true);
            this.indelsBiasSOR = computeStrandBiasSOR(refHasIndelsCount,refHasNotIndelsCount,altHasIndelsCount,altHasNotIndelsCount,true);

            this.altAlellesOdds = (double)(this.altAlleleCount+1)/(double)(this.otherAltAlleleCount+1);
            this.altAlellesFreqDiff = ((this.altAlleleCount+1)/(double)(this.dp+1)) - ((this.otherAltAlleleCount+1)/(double)(this.dp+1));


            // by allele
            if(refIndexes.size()>0) {
                this.meanBaseQualitiesErrorProbabilityRef = computeMeanErrorProbability(this.baseQualities, refIndexes);
                this.meanMappingQualitiesErrorProbabilityRef = computeMeanErrorProbability(this.mappingQualities, refIndexes);
            }
            if(altIndexes.size()>0) {
                this.meanBaseQualitiesErrorProbabilityAlt = computeMeanErrorProbability(this.baseQualities, altIndexes);
                this.meanMappingQualitiesErrorProbabilityAlt = computeMeanErrorProbability(this.mappingQualities, altIndexes);
            }

        }

    }

    private float computeMeanErrorProbability(List<Byte> values, List<Integer> indexes){
        List<Byte> bValues = new ArrayList<>(indexes.size());
        for(int i=0; i<indexes.size(); i++){
            bValues.add(values.get(indexes.get(i)));
        }
        return computeMeanErrorProbability(bValues);
    }

    private float computeMeanErrorProbability(List<Byte> values){
        double acumValue = 0;
        for(Byte value:values){
            acumValue += phred2prob(value);
        }
        return (float)(acumValue/(double)values.size());
    }

    private double computeMannWhitneyU(List values, List<Integer> refIndexes, List<Integer> altIndexes){
        if(refIndexes.size()==0 || altIndexes.size()==0){
            return 0;
        } else {
            double[] refValues = prepareSubset(values, refIndexes);
            //System.err.println("ref:" + Arrays.toString(refValues));
            double[] altValues = prepareSubset(values, altIndexes);
            //System.err.println("alt:" + Arrays.toString(altValues));
            MannWhitneyUTest mwtester = new MannWhitneyUTest();
            double u = mwtester.mannWhitneyU(altValues, refValues);


            int n1 = refIndexes.size();
            int n2 = altIndexes.size();
            double mu = (float) (n1 * n2) / 2.0;
            double sdu = Math.sqrt((float) (n1 * n2 * (n1 + n2 + 1)) / 12.0);
            double z = (float) (u - mu) / (float) (sdu);
            double u2 = mwtester.mannWhitneyU(refValues,altValues);
            double z2 = (float) (u2 - mu) / (float) (sdu);
            //System.err.println("u:" + u + " n1:" + n1 + " n2:" + n2 + " mu:" + mu + " sdu:" + sdu + " z:" + z + " u2:" + u2 + " z2:" + z2 + "\n");
            return z;
        }
    }

    private double computeTTest(List values, List<Integer> refIndexes, List<Integer> altIndexes){
        if(refIndexes.size()==0 || altIndexes.size()==0){
            return 0;
        } else {
            double[] refValues = prepareSubset(values, refIndexes);
            //System.err.println("ref:" + Arrays.toString(refValues));
            double[] altValues = prepareSubset(values, altIndexes);
            //System.err.println("alt:" + Arrays.toString(altValues));

            TTest tester = new TTest();
            double u;
            try {
                u = tester.t(altValues, refValues);
            }catch(NumberIsTooSmallException e){
                u = 0;
            }
            return u;
        }
    }

    private double computeStrandBiasSOR(int refPosCount, int refNegCount, int altPosCount, int altNegCount, boolean correct){
        if(correct){
            refPosCount++;
            refNegCount++;
            altPosCount++;
            altNegCount++;
        }
        double R = (double)(refPosCount*altNegCount)/(double)(refNegCount*altPosCount);
        return R + 1.0/R;
    }

    private double[] prepareSubset(List<Object> values, List<Integer> indexes){
        double[] subset = new double[indexes.size()];
        for(int i=0; i<indexes.size(); i++){
            if(values.get(indexes.get(i)) instanceof Byte){
                subset[i] = ((Byte) values.get(indexes.get(i))).doubleValue();
            }
            if(values.get(indexes.get(i)) instanceof Integer){
                subset[i] = ((Integer) values.get(indexes.get(i))).doubleValue();
            }

        }
        return subset;
    }

    private double phred2prob(byte phred){
        return Math.pow(10,((float)(-phred)/10.0));
    }

    public static double[] initFactorialTable(int maxSize){
        double cf = 1.0;
        double[]f = new double[maxSize + 1];
        f[0] = 0.0;
        for (int i = 1; i <= maxSize; i++) {
            f[i] = f[i - 1] + Math.log(i);
        }
        return f;
    }

//    public String toSimpleString(){
//        return this.chr + ":" + this.start + "-" + this.end + "  dp=" + this.dp + " " +
//                this.refAllele +  ":" + this.refAlleleCount + " " +
//                this.altAllele +  ":" + this.altAlleleCount + " " +
//                this.otherAltAlleles +  ":" + this.otherAltAlleleCount + " " +
//                this.allelesCount;
//    }
//
//    public String header(){
//        return header(true);
//    }

//    public String header(boolean printVariantInfo){
//        String samplePrefix = this.sampleName;
//        if(!samplePrefix.equals("")) samplePrefix = samplePrefix + ".";
//        String vi = "";
//        if(printVariantInfo) vi = "chr" + sep + "pos" + sep + "ref" + sep;
//        return  vi + samplePrefix + "alt" + sep + samplePrefix + "other_alt" + sep +
//                samplePrefix + "dp" + sep + samplePrefix + "ref_count" + sep + samplePrefix + "alt_count" + sep + samplePrefix + "other_alt_count" + sep +
//                samplePrefix + "ref_freq" + sep + samplePrefix + "alt_freq" + sep + samplePrefix + "other_alt_freq" + sep +
//                samplePrefix + "alts_odds" + sep + samplePrefix + "alts_freqs_diff" + sep +
//                samplePrefix + "mean_bq_prob" + sep + samplePrefix + "mean_bq_prob_ref" + sep + samplePrefix + "mean_bq_prob_alt" + sep +
//                samplePrefix + "bq_zero_count" + sep + samplePrefix + "bq_zero_ref_count" + sep + samplePrefix + "bq_zero_alt_count" + sep +
//                samplePrefix + "bq_zero_freq" + sep + samplePrefix + "bq_zero_ref_freq" + sep + samplePrefix + "bq_zero_alt_freq" + sep +
//                samplePrefix + "bq_ten_count" + sep + samplePrefix + "bq_ten_ref_count" + sep + samplePrefix + "bq_ten_alt_count" + sep +
//                samplePrefix + "bq_ten_freq" + sep + samplePrefix + "bq_ten_ref_freq" + sep + samplePrefix + "bq_ten_alt_freq" + sep +
//                samplePrefix + "mean_mq_prob" + sep + samplePrefix + "mean_mq_prob_ref" + sep + samplePrefix + "mean_mq_prob_alt" + sep +
//                samplePrefix + "mq_zero_count" + sep + samplePrefix + "mq_zero_ref_count" + sep + samplePrefix + "mq_zero_alt_count" + sep +
//                samplePrefix + "mq_zero_freq" + sep + samplePrefix + "mq_zero_ref_freq" + sep + samplePrefix + "mq_zero_alt_freq" + sep +
//                samplePrefix + "mq_ten_count" + sep + samplePrefix + "mq_ten_ref_count" + sep + samplePrefix + "mq_ten_alt_count" + sep +
//                samplePrefix + "mq_ten_freq" + sep + samplePrefix + "mq_ten_ref_freq" + sep + samplePrefix + "mq_ten_alt_freq" + sep +
//                samplePrefix + "bq_ranksum_z" + sep + samplePrefix + "mq_ranksum_z" + sep + samplePrefix + "rp_ranksum_z" + sep +
//                samplePrefix + "strand_bias_sor" + sep +
//                samplePrefix + "clipping_bias_sor" + sep + samplePrefix + "indels_bias_sor" + sep + samplePrefix + "cigar_ranksum_z";
//    }

//    public String toString(){
//        return toString(true);
//    }

//    public String toString(boolean printVariantInfo){
//        String vi = "";
//        if(printVariantInfo) vi = this.chr + sep + this.start + sep +  this.refAllele + sep;
//        return  vi + this.altAllele + sep + this.otherAltAlleles + sep +
//                this.dp + sep + this.refAlleleCount + sep + this.altAlleleCount + sep + this.otherAltAlleleCount + sep +
//                this.refAlleleFrequency + sep + this.altAlleleFrequency + sep + this.otherAltAlleleFrequency + sep +
//                this.altAlellesOdds + sep + this.altAlellesFreqDiff + sep +
//                this.meanBaseQualitiesErrorProbability + sep + this.meanBaseQualitiesErrorProbabilityRef + sep + this.meanBaseQualitiesErrorProbabilityAlt + sep +
//                this.baseQualityZeroCount + sep + this.baseQualityZeroCountRef + sep + this.baseQualityZeroCountAlt + sep +
//                this.baseQualityZeroFrequency + sep + this.baseQualityZeroFrequencyRef + sep + this.baseQualityZeroFrequencyAlt + sep +
//                this.baseQualityTenCount + sep + this.baseQualityTenCountRef + sep + this.baseQualityTenCountAlt + sep +
//                this.baseQualityTenFrequency + sep + this.baseQualityTenFrequencyRef + sep + this.baseQualityTenFrequencyAlt + sep +
//                this.meanMappingQualitiesErrorProbability + sep + this.meanMappingQualitiesErrorProbabilityRef + sep + this.meanMappingQualitiesErrorProbabilityAlt + sep +
//                this.mappingQualityZeroCount + sep + this.mappingQualityZeroCountRef + sep + this.mappingQualityZeroCountAlt + sep +
//                this.mappingQualityZeroFrequency + sep + this.mappingQualityZeroFrequencyRef + sep + this.mappingQualityZeroFrequencyAlt + sep +
//                this.mappingQualityTenCount + sep + this.mappingQualityTenCountRef + sep + this.mappingQualityTenCountAlt + sep +
//                this.mappingQualityTenFrequency + sep + this.mappingQualityTenFrequencyRef + sep + this.mappingQualityTenFrequencyAlt + sep +
//                this.baseQualityRankSumZ + sep + this.mappingQualityRankSumZ + sep + this.readPositionRankSumZ + sep +
//                this.strandBiasSOR + sep +
//                this.clippingBiasSOR + sep + this.indelsBiasSOR + sep + this.numberOfCigarElementsRankSumZ;
//    }

//    public String toString(boolean printVariantInfo){
//        String vi = "";
//        if(printVariantInfo) vi = this.chr + sep + this.start + sep +  this.refAllele + sep;
//        return  vi + this.altAllele + sep + this.otherAltAlleles + sep +
//                this.dp + sep + this.refAlleleCount + sep + this.altAlleleCount + sep + this.otherAltAlleleCount + sep +
//                round(this.refAlleleFrequency) + sep + round(this.altAlleleFrequency) + sep + round(this.otherAltAlleleFrequency) + sep +
//                round(this.altAlellesOdds) + sep + round(this.altAlellesFreqDiff) + sep +
//                round(this.meanBaseQualitiesErrorProbability) + sep + round(this.meanBaseQualitiesErrorProbabilityRef) + sep + round(this.meanBaseQualitiesErrorProbabilityAlt) + sep +
//                this.baseQualityZeroCount + sep + this.baseQualityZeroCountRef + sep + this.baseQualityZeroCountAlt + sep +
//                round(this.baseQualityZeroFrequency) + sep + round(this.baseQualityZeroFrequencyRef) + sep + round(this.baseQualityZeroFrequencyAlt) + sep +
//                this.baseQualityTenCount + sep + this.baseQualityTenCountRef + sep + this.baseQualityTenCountAlt + sep +
//                round(this.baseQualityTenFrequency) + sep + round(this.baseQualityTenFrequencyRef) + sep + round(this.baseQualityTenFrequencyAlt) + sep +
//                round(this.meanMappingQualitiesErrorProbability) + sep + round(this.meanMappingQualitiesErrorProbabilityRef) + sep + round(this.meanMappingQualitiesErrorProbabilityAlt) + sep +
//                this.mappingQualityZeroCount + sep + this.mappingQualityZeroCountRef + sep + this.mappingQualityZeroCountAlt + sep +
//                round(this.mappingQualityZeroFrequency) + sep + round(this.mappingQualityZeroFrequencyRef) + sep + round(this.mappingQualityZeroFrequencyAlt) + sep +
//                this.mappingQualityTenCount + sep + this.mappingQualityTenCountRef + sep + this.mappingQualityTenCountAlt + sep +
//                round(this.mappingQualityTenFrequency) + sep + round(this.mappingQualityTenFrequencyRef) + sep + round(this.mappingQualityTenFrequencyAlt) + sep +
//                round(this.baseQualityRankSumZ) + sep + round(this.mappingQualityRankSumZ) + sep + round(this.readPositionRankSumZ) + sep +
//                round(this.strandBiasSOR) + sep +
//                round(this.clippingBiasSOR) + sep + round(this.indelsBiasSOR) + sep + round(this.numberOfCigarElementsRankSumZ);
//    }

    public char getRefAllele() {
        return refAllele;
    }

    private double round(double value){
        return (double)Math.round(value * 100000d) / 100000d;
    }

    public int getDp() {
        return dp;
    }

    public int getRefAlleleCount() {
        return refAlleleCount;
    }

    public int getAltAlleleCount() {
        return altAlleleCount;
    }

    public int getOtherAltAlleleCount() {
        return otherAltAlleleCount;
    }

    public float getRefAlleleFrequency() {
        return refAlleleFrequency;
    }

    public float getAltAlleleFrequency() {
        return altAlleleFrequency;
    }

    public float getOtherAltAlleleFrequency() {
        return otherAltAlleleFrequency;
    }

    public int getMappingQualityZeroCount() {
        return mappingQualityZeroCount;
    }

    public int getMappingQualityZeroCountAlt() {
        return mappingQualityZeroCountAlt;
    }

    public int getMappingQualityZeroCountRef() {
        return mappingQualityZeroCountRef;
    }

    public float getMappingQualityZeroFrequency() {
        return mappingQualityZeroFrequency;
    }

    public float getMappingQualityZeroFrequencyAlt() {
        return mappingQualityZeroFrequencyAlt;
    }

    public float getMappingQualityZeroFrequencyRef() {
        return mappingQualityZeroFrequencyRef;
    }

    public int getBaseQualityZeroCount() {
        return baseQualityZeroCount;
    }

    public int getBaseQualityZeroCountAlt() {
        return baseQualityZeroCountAlt;
    }

    public int getBaseQualityZeroCountRef() {
        return baseQualityZeroCountRef;
    }

    public float getBaseQualityZeroFrequency() {
        return baseQualityZeroFrequency;
    }

    public float getBaseQualityZeroFrequencyAlt() {
        return baseQualityZeroFrequencyAlt;
    }

    public float getBaseQualityZeroFrequencyRef() {
        return baseQualityZeroFrequencyRef;
    }

    public int getMappingQualityTenCount() {
        return mappingQualityTenCount;
    }

    public int getMappingQualityTenCountAlt() {
        return mappingQualityTenCountAlt;
    }

    public int getMappingQualityTenCountRef() {
        return mappingQualityTenCountRef;
    }

    public float getMappingQualityTenFrequency() {
        return mappingQualityTenFrequency;
    }

    public float getMappingQualityTenFrequencyAlt() {
        return mappingQualityTenFrequencyAlt;
    }

    public float getMappingQualityTenFrequencyRef() {
        return mappingQualityTenFrequencyRef;
    }

    public int getBaseQualityTenCount() {
        return baseQualityTenCount;
    }

    public int getBaseQualityTenCountAlt() {
        return baseQualityTenCountAlt;
    }

    public int getBaseQualityTenCountRef() {
        return baseQualityTenCountRef;
    }

    public float getBaseQualityTenFrequency() {
        return baseQualityTenFrequency;
    }

    public float getBaseQualityTenFrequencyAlt() {
        return baseQualityTenFrequencyAlt;
    }

    public float getBaseQualityTenFrequencyRef() {
        return baseQualityTenFrequencyRef;
    }

    public double getMeanBaseQualitiesErrorProbability() {
        return meanBaseQualitiesErrorProbability;
    }

    public double getMeanBaseQualitiesErrorProbabilityAlt() {
        return meanBaseQualitiesErrorProbabilityAlt;
    }

    public double getMeanBaseQualitiesErrorProbabilityRef() {
        return meanBaseQualitiesErrorProbabilityRef;
    }

    public double getMeanMappingQualitiesErrorProbability() {
        return meanMappingQualitiesErrorProbability;
    }

    public double getMeanMappingQualitiesErrorProbabilityAlt() {
        return meanMappingQualitiesErrorProbabilityAlt;
    }

    public double getMeanMappingQualitiesErrorProbabilityRef() {
        return meanMappingQualitiesErrorProbabilityRef;
    }

    public double getBaseQualityRankSumZ() {
        return baseQualityRankSumZ;
    }

    public double getMappingQualityRankSumZ() {
        return mappingQualityRankSumZ;
    }

    public double getReadPositionRankSumZ() {
        return readPositionRankSumZ;
    }

    public double getStrandBiasSOR() {
        return strandBiasSOR;
    }

    public double getAltAlellesOdds() {
        return altAlellesOdds;
    }

    public double getAltAlellesFreqDiff() {
        return altAlellesFreqDiff;
    }

    public double getClippingBiasSOR() {
        return clippingBiasSOR;
    }

    public double getIndelsBiasSOR() {
        return indelsBiasSOR;
    }

    public double getNumberOfCigarElementsRankSumZ() {
        return numberOfCigarElementsRankSumZ;
    }

    public boolean isReference() {
        return isReference;
    }

    public boolean isHeterozygous() {
        return isHeterozygous;
    }

    public boolean isHomozygous() {
        return isHomozygous;
    }

    public boolean isVariant() {
        return isVariant;
    }

    public boolean isAlternativeHomozygous() {
        return isAlternativeHomozygous;
    }

    public float getAllelicFrequency() {
        return allelicFrequency;
    }

    public List<Character> getBases() {
        return bases;
    }

    public List<Boolean> getStrands() {
        return strands;
    }

    public List<Byte> getBaseQualities() {
        return baseQualities;
    }

    public List<Byte> getMappingQualities() {
        return mappingQualities;
    }

    public List<Integer> getReadPositions() {
        return readPositions;
    }

    public List<Integer> getNumberOfCigarElements() {
        return numberOfCigarElements;
    }

    public List<Boolean> getIsClipped() {
        return isClipped;
    }

    public List<Boolean> getHasIndels() {
        return hasIndels;
    }

    public List<Boolean> getIsDuplicated() {
        return isDuplicated;
    }

    public List<Boolean> getIsNotPrimaryAlignment() {
        return isNotPrimaryAlignment;
    }

    public List<Boolean> getIsProperlyPaired() {
        return isProperlyPaired;
    }

    public List<Boolean> getIsMateUnmapped() {
        return isMateUnmapped;
    }

    public int getIndelsCount() {
        return indelsCount;
    }

    public int getClippedCount() {
        return clippedCount;
    }

    public int getDuplicatedCount() {
        return duplicatedCount;
    }

    public int getNotPrimaryAlignmentCount() {
        return notPrimaryAlignmentCount;
    }

    public int getProperlyPairedCount() {
        return properlyPairedCount;
    }

    public int getMateUnmappedCount() {
        return mateUnmappedCount;
    }

    public float getIndelsFrequency() {
        return indelsFrequency;
    }

    public float getClippedFrequency() {
        return clippedFrequency;
    }

    public float getDuplicatedFrequency() {
        return duplicatedFrequency;
    }

    public float getNotPrimaryAlignmentFrequency() {
        return notPrimaryAlignmentFrequency;
    }

    public float getProperlyPairedFrequency() {
        return properlyPairedFrequency;
    }

    public float getMateUnmappedFrequency() {
        return mateUnmappedFrequency;
    }
}
