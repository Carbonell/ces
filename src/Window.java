
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.lang.reflect.Field;
//import GenomeProfiler.WindowSummaryOptions;

public class Window {

    public static String separator = "\t";

    private String chr;
    private int start;
    private int end;
    private int windowSize;
    private HashMap<String,Double> parameterList;
    private List<String> parameterNames;
    private GenomeProfiler.WindowSummaryOptions windowSummary;
    double harmonicMean;

    public Window(String chr, int start, int end, GenomeProfiler.WindowSummaryOptions windowSummary){
        this.chr = chr;
        this.start = start;
        this.end = end;
        this.windowSize = end-start+1;
        this.windowSummary = windowSummary;
    }

    public void processWindow(Batch batches[]) throws NoSuchFieldException, IllegalAccessException {

        parameterList = new HashMap<>();
        parameterNames = new ArrayList();

        // general parameters
        summarizeAndStoreParameter("baseQualityZeroFrequency",false,batches);
        summarizeAndStoreParameter("baseQualityTenFrequency",false,batches);
        summarizeAndStoreParameter("meanBaseQualitiesErrorProbability",false,batches);
        summarizeAndStoreParameter("mappingQualityZeroFrequency",false,batches);
        summarizeAndStoreParameter("mappingQualityTenFrequency",false,batches);
        summarizeAndStoreParameter("meanMappingQualitiesErrorProbability",false,batches);
        summarizeAndStoreParameter("allelicFrequency",false,batches);

        summarizeAndStoreParameter("dp",false,batches);
        summarizeAndStoreParameter("refAlleleCount",false,batches);
        summarizeAndStoreParameter("altAlleleCount",false,batches);
        summarizeAndStoreParameter("otherAltAlleleCount",false,batches);

        summarizeAndStoreParameter("refAlleleFrequency",false,batches);
        summarizeAndStoreParameter("altAlleleFrequency",false,batches);
        summarizeAndStoreParameter("otherAltAlleleFrequency",false,batches);

        // variant-based parameters
        summarizeAndStoreParameter("baseQualityRankSumZ",true,batches);
        summarizeAndStoreParameter("mappingQualityRankSumZ",true,batches);
        summarizeAndStoreParameter("readPositionRankSumZ",true,batches);
        summarizeAndStoreParameter("numberOfCigarElementsRankSumZ",true,batches);

        //summarizeAndStoreParameter("baseQualityTTest",true,batches);
        //summarizeAndStoreParameter("mappingQualityTTest",true,batches);
        //summarizeAndStoreParameter("readPositionTTest",true,batches);
        //summarizeAndStoreParameter("numberOfCigarElementsTTest",true,batches);

        summarizeAndStoreParameter("strandBiasSOR",true,batches);
        summarizeAndStoreParameter("altAlellesOdds",true,batches);
        summarizeAndStoreParameter("altAlellesFreqDiff",true,batches);

        summarizeAndStoreParameter("clippingBiasSOR",true,batches);
        summarizeAndStoreParameter("indelsBiasSOR",true,batches);

        // population genetics
        summarizeAndStoreNucleotideDiversity(batches);
        summarizeAndStoreHeterozygosity(batches);
        this.harmonicMean = computeHarmonicMean(batches.length);
        summarizeAndStoreWattersonEstimator(batches);
        summarizeAndStoreParsimonyInformativeSite(batches);
        summarizeAndStoreBooleanParameter("isAlternativeHomozygous",false,batches);
        summarizeAndStoreBooleanParameter("isHeterozygous",false,batches);

        // other counts
        summarizeAndStoreParameter("indelsCount",false,batches);
        summarizeAndStoreParameter("clippedCount",false,batches);
        summarizeAndStoreParameter("duplicatedCount",false,batches);
        summarizeAndStoreParameter("notPrimaryAlignmentCount",false,batches);
        summarizeAndStoreParameter("properlyPairedCount",false,batches);
        summarizeAndStoreParameter("mateUnmappedCount",false,batches);

        summarizeAndStoreParameter("indelsFrequency",false,batches);
        summarizeAndStoreParameter("clippedFrequency",false,batches);
        summarizeAndStoreParameter("duplicatedFrequency",false,batches);
        summarizeAndStoreParameter("notPrimaryAlignmentFrequency",false,batches);
        summarizeAndStoreParameter("properlyPairedFrequency",false,batches);
        summarizeAndStoreParameter("mateUnmappedFrequency",false,batches);



    }

    private void summarizeAndStoreParameter(String parameterName, boolean mustBeTraced, Batch batches[]) throws NoSuchFieldException, IllegalAccessException {
        parameterNames.add(parameterName);
        parameterList.put(parameterName,new Double(summarizeParameter(parameterName,mustBeTraced,batches)));
    }

    private double summarizeParameter(String parameterName, boolean mustBeTraced, Batch batches[]) throws NoSuchFieldException, IllegalAccessException {

        double baseValues[] = new double[this.windowSize];
        boolean baseValidValues[] = new boolean[this.windowSize];
        double sampleValues[];
        boolean sampleValidValues[];

        for(int i=0; i<this.windowSize; i++) {

            // sample value for a given base (p_ij)
            sampleValues = getSampleDoubleValues(parameterName, batches, i);
            sampleValidValues = getSampleValidValues(mustBeTraced,batches,i);

            // base summary (ri)
            baseValues[i] = simpleMean(sampleValues,sampleValidValues);
            baseValidValues[i] = any(sampleValidValues) | !mustBeTraced;
        }

        // window summary
        double windowValue = Double.NaN;
        switch (this.windowSummary){
            case MEAN:
                windowValue = simpleMean(baseValues, baseValidValues);
                break;
            case LINEAR:
                windowValue = weightMean(baseValues, baseValidValues);
                break;
        }

        return(windowValue);
    }

    private void summarizeAndStoreBooleanParameter(String parameterName, boolean mustBeTraced, Batch batches[]) throws NoSuchFieldException, IllegalAccessException {
        parameterNames.add(parameterName);
        parameterList.put(parameterName,new Double(summarizeBooleanParameter(parameterName,mustBeTraced,batches)));
    }

    private double summarizeBooleanParameter(String parameterName, boolean mustBeTraced, Batch batches[]) throws NoSuchFieldException, IllegalAccessException {

        double baseValues[] = new double[this.windowSize];
        boolean baseValidValues[] = new boolean[this.windowSize];
        boolean sampleValues[];
        boolean sampleValidValues[];

        for(int i=0; i<this.windowSize; i++) {

            // sample value for a given base (p_ij)
            sampleValues = getSampleBooleanValues(parameterName, batches, i);
            sampleValidValues = getSampleValidValues(mustBeTraced,batches,i);

            // base summary (ri)
            baseValues[i] = simpleMean(sampleValues,sampleValidValues);
            baseValidValues[i] = any(sampleValidValues) | !mustBeTraced;
        }

        // window summary
        double windowValue = Double.NaN;
        switch (this.windowSummary){
            case MEAN:
                windowValue = simpleMean(baseValues, baseValidValues);
                break;
            case LINEAR:
                windowValue = weightMean(baseValues, baseValidValues);
                break;
        }

        return(windowValue);
    }



    private void summarizeAndStoreNucleotideDiversity(Batch batches[]) throws NoSuchFieldException, IllegalAccessException {
        parameterNames.add("nucleotideDiversity");
        parameterList.put("nucleotideDiversity",new Double(summarizeNucleotideDiversity(batches)));
    }
    private double summarizeNucleotideDiversity(Batch batches[]) throws NoSuchFieldException, IllegalAccessException {

        double baseValues[] = new double[this.windowSize];;

        double sampleValues[];
        double xij = 1/(double)batches.length;

        for(int i=0; i<this.windowSize; i++){

            sampleValues = getSampleDoubleValues("allelicFrequency", batches, i);

            double acumDiversity = 0;
            for(int j=0; j<batches.length; j++){
                for(int k=0; k<(j-1); k++){
                    if(sampleValues[j]!=sampleValues[k]){
                        acumDiversity += xij*xij;
                    }
                }
            }

            baseValues[i] = acumDiversity*2;

        }

        // window summary
        double windowValue = Double.NaN;
        switch (this.windowSummary){
            case MEAN:
                windowValue = simpleMean(baseValues);
                break;
            case LINEAR:
                windowValue = weightMean(baseValues);
                break;
        }

        return(windowValue);

    }

    private void summarizeAndStoreHeterozygosity(Batch batches[]) throws NoSuchFieldException, IllegalAccessException {
        parameterNames.add("heterozygosity");
        parameterList.put("heterozygosity",new Double(summarizeHeterozygosity(batches)));
    }
    private double summarizeHeterozygosity(Batch batches[]) throws NoSuchFieldException, IllegalAccessException {

        double baseValues[] = new double[this.windowSize];

        double aaf, raf;

        for (int i = 0; i < this.windowSize; i++) {

            aaf = simpleMean(getSampleDoubleValues("allelicFrequency", batches, i));
            raf = 1 - aaf;

            baseValues[i] = 1.0 - raf * raf + aaf * aaf;

        }

        // window summary
        double windowValue = Double.NaN;
        switch (this.windowSummary){
            case MEAN:
                windowValue = simpleMean(baseValues);
                break;
            case LINEAR:
                windowValue = weightMean(baseValues);
                break;
        }

        return(windowValue);

    }


    private void summarizeAndStoreWattersonEstimator(Batch batches[]) throws NoSuchFieldException, IllegalAccessException {
        parameterNames.add("wattersonEstimator");
        parameterList.put("wattersonEstimator",new Double(summarizeWattersonEstimator(batches)));
    }
    private double summarizeWattersonEstimator(Batch batches[]) throws NoSuchFieldException, IllegalAccessException {

        double baseValues[] = new double[this.windowSize];

        for (int i = 0; i < this.windowSize; i++) {

            if(any(getSampleBooleanValues("isVariant", batches, i))){
                baseValues[i] = this.harmonicMean;
            }

        }

        // window summary
        double windowValue = Double.NaN;
        switch (this.windowSummary){
            case MEAN:
                windowValue = simpleMean(baseValues);
                break;
            case LINEAR:
                windowValue = weightMean(baseValues);
                break;
        }

        return(windowValue);

    }

    private void summarizeAndStoreParsimonyInformativeSite(Batch batches[]) throws NoSuchFieldException, IllegalAccessException {
        parameterNames.add("parsimonyInformativeSite");
        parameterList.put("parsimonyInformativeSite",new Double(summarizeParsimonyInformativeSite(batches)));
    }
    private double summarizeParsimonyInformativeSite(Batch batches[]) throws NoSuchFieldException, IllegalAccessException {

        double baseValues[] = new double[this.windowSize];
        double aaf;
        double one = 1/(double)batches.length;
        for (int i = 0; i < this.windowSize; i++) {

            aaf = simpleMean(getSampleDoubleValues("allelicFrequency", batches, i));

            if(aaf>=one & aaf<=(1-one)){
                baseValues[i] = 1;
            }

        }

        // window summary
        double windowValue = Double.NaN;
        switch (this.windowSummary){
            case MEAN:
                windowValue = simpleMean(baseValues);
                break;
            case LINEAR:
                windowValue = weightMean(baseValues);
                break;
        }

        return(windowValue);

    }


    private boolean any(boolean[] validValues){
        boolean any = false;
        for(int i=0; i<validValues.length; i++){
            if(validValues[i]) {
                any = true;
                break;
            }
        }
        return any;
    }

    private boolean[] getSampleValidValues(boolean mustBeTraced, Batch batches[], int baseIndex) throws NoSuchFieldException, IllegalAccessException {
        boolean validValues[] = new boolean[batches.length];
        if(mustBeTraced){
            validValues = getSampleBooleanValues("isVariant", batches, baseIndex);
        } else {
            Arrays.fill(validValues, true);
        }
        return validValues;
    }


    private double[] getSampleDoubleValues(String parameterName, Batch batches[], int baseIndex) throws NoSuchFieldException, IllegalAccessException {

        double values[] = new double[batches.length];
        Field field;
        PileupRecord pr;
        for(int i=0; i<batches.length; i++){
            pr = batches[i].getPileup(baseIndex);
            field = pr.getClass().getDeclaredField(parameterName);
            field.setAccessible(true);
            values[i] = field.getDouble(pr);
        }

        return values;
    }

    private boolean[] getSampleBooleanValues(String parameterName, Batch batches[], int baseIndex) throws NoSuchFieldException, IllegalAccessException {

        boolean values[] = new boolean[batches.length];
        Field field;
        PileupRecord pr;
        for(int i=0; i<batches.length; i++){
            pr = batches[i].getPileup(baseIndex);
            field = pr.getClass().getDeclaredField(parameterName);
            field.setAccessible(true);
            values[i] = field.getBoolean(pr);
        }

        return values;
    }


    private double simpleMean(double values[]){
        boolean validValues[] = new boolean[values.length];
        Arrays.fill(validValues, true);
        return(this.simpleMean(values,validValues));
    }

    private double simpleMean(double values[],boolean validValues[]){

        double acum = 0;
        int n = 0;
        for(int i=0; i<values.length; i++){
            if(validValues[i]) {
                acum += values[i];
                n++;
            }
        }
        return(round(acum/(double)n));
    }

    private double simpleMean(boolean values[]){
        boolean validValues[] = new boolean[values.length];
        Arrays.fill(validValues, true);
        return(this.simpleMean(values,validValues));
    }

    private double simpleMean(boolean values[],boolean validValues[]){

        double acum = 0;
        int n = 0;
        for(int i=0; i<values.length; i++){
            if(validValues[i] && values[i]) {
                acum++;
                n++;
            }
        }
        return(round(acum/(double)n));
    }

    private double weightMean(double values[]){
        boolean validValues[] = new boolean[values.length];
        Arrays.fill(validValues, true);
        return(this.weightMean(values,validValues));
    }
    private double weightMean(double values[],boolean validValues[]){

        // compute weights
        int center = (int)Math.floor((double)values.length/2.0);
        double weights[] = new double[values.length];
        double acumWeights = 0;
        for(int i=0; i<values.length; i++) {
            if(validValues[i]) {
                weights[i] = center - Math.abs(i-center);
                acumWeights += weights[i];
            }
        }
        for(int i=0; i<values.length; i++) {
            if (validValues[i]) {
                weights[i] = weights[i]/acumWeights;
            }
        }

        // compute window value
        double acum = 0;
        for(int i=0; i<values.length; i++){
            if(validValues[i]) {
                acum += values[i]*weights[i];
            }
        }

        return(acum);

    }

    public String header(){
        StringBuffer out = new StringBuffer();
        out.append("#chr").append(this.separator);
        out.append("start").append(this.separator);
        out.append("end");
        for(String key:parameterNames){
            out.append(this.separator).append(key);
        }
        return out.toString();
    }

    public String toString(){
        StringBuffer out = new StringBuffer();
        out.append(chr).append(this.separator);
        out.append(start).append(this.separator);
        out.append(end);
        for(String key:parameterNames){
            out.append(this.separator).append(parameterList.get(key));
        }
        return out.toString();
    }

    private double computeHarmonicMean(int n){
        double hm = 0;
        for(int i=0; i<n; i++){
            hm += 1/(double)i;
        }
        return(hm);
    }

    private double round(double value){
        return (double)Math.round(value * 1000000d) / 1000000d;
    }

    public HashMap<String, Double> getParameterList() {
        return parameterList;
    }

    public List<String> getParameterNames() {
        return parameterNames;
    }
}
