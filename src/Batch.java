
import htsjdk.samtools.CigarElement;
import htsjdk.samtools.SAMRecord;

public class Batch {

    private String sampleName;

    private String chr;
    private int start;
    private int end;

    private byte[] refAlleles;

    private int length;

    private PileupRecord[] pileups;

    public Batch(String sampleName, String chr, int start, int end, byte[] refAlleles){

        this.sampleName = sampleName;

        this.chr = chr;
        this.start = start;
        this.end = end;
        this.refAlleles = refAlleles;

        this.length = this.end - this.start + 1;

        this.initPileups();

    }

    private void initPileups(){
        this.pileups = new PileupRecord[this.length];
        for(int i=0; i<this.length; i++){
            this.pileups[i] = new PileupRecord(this.sampleName,this.chr,this.start+i,this.start+i,(char)this.refAlleles[i]);
        }
    }

    public void processRead(SAMRecord samRecord){
        int index;
        int readPos;
        //boolean isClipped = samRecord.getAlignmentStart()!=samRecord.getUnclippedStart() | samRecord.getAlignmentEnd()!=samRecord.getUnclippedEnd();
        boolean hasIndels = false;
        for(CigarElement ce: samRecord.getCigar().getCigarElements()){
            if(ce.getOperator().isIndel()) hasIndels = true;
        }
        for(int pos=samRecord.getAlignmentStart(); pos<=samRecord.getAlignmentEnd(); pos++){
            index = pos-this.start;
            if(index>0 & index<this.length) {
                readPos = samRecord.getReadPositionAtReferencePosition(pos) - 1;
                if(readPos!=-1) { // INDELS!!!!!!!

                    boolean properPairFlag = true;
                    boolean mateUnmappedFlag = false;
                    if(samRecord.getReadPairedFlag()) {
                        properPairFlag = samRecord.getProperPairFlag();
                        mateUnmappedFlag = samRecord.getMateUnmappedFlag();
                    }

                    //System.err.println("prueba: " + samRecord.getContig() + ":" + samRecord.getStart() + "-" + samRecord.getEnd() + "  " + properPairFlag);

                    pileups[index].addReadBaseInfo(
                            (char) samRecord.getReadBases()[readPos],
                            !samRecord.getReadNegativeStrandFlag(),
                            samRecord.getBaseQualities()[readPos],
                            (byte) samRecord.getMappingQuality(),
                            readPos+1,
                            samRecord.getCigar().isClipped(),
                            hasIndels,
                            samRecord.getCigarLength(),
                            samRecord.getDuplicateReadFlag(),
                            samRecord.getNotPrimaryAlignmentFlag(),
                            properPairFlag,
                            mateUnmappedFlag
                    );

                }
            }
        }
    }

//    public void print(boolean printHeader, boolean printVariantInfo){
//        if(printHeader) System.out.println(header(printVariantInfo));
//        for(int i=0; i<this.length; i++) System.out.println(pileups[i].toString());
//    }

    public void summarize() {
        for(PileupRecord pr:pileups){
            pr.summarize();
        }
    }

//    public String header(){
//        return header(true);
//    }
//
//    public String header(boolean printVariantInfo) {
//        return pileups[0].header(printVariantInfo);
//    }

    public int getLength() {
        return length;
    }

    public PileupRecord[] getPileups() {
        return pileups;
    }

    public PileupRecord getPileup(int index) {
        return pileups[index];
    }
}
