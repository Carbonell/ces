import htsjdk.samtools.*;
import htsjdk.samtools.util.BlockCompressedOutputStream;
import htsjdk.samtools.reference.IndexedFastaSequenceFile;
import htsjdk.tribble.bed.BEDFeature;
import htsjdk.tribble.index.tabix.TabixFormat;
import htsjdk.tribble.index.tabix.TabixIndexCreator;
import htsjdk.tribble.index.Index;
import htsjdk.tribble.util.TabixUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class GenomeProfiler {

    public enum WindowSummaryOptions {MEAN, LINEAR}

    private WindowSummaryOptions windowSummary;

    private String refFile;
    private String samplesFile;
    private List<String> bamFiles;
    private List<String> sampleNames;
    private int numberOfSamples;
    private int contextSize = 10;

    private SamReader[] bamReaders;
    private IndexedFastaSequenceFile refReader;

    private String region;
    private String bedFile;
    private List<Region> regions;
    private List<Region> genomeContigs;
    private Map<String,Integer> genomeContigsLength;

    private ParameterOverview parameterOverview;

    private int windowSize = 100;
    private int bins = 1000;

    private String outputFolder;
    private PrintWriter profileWriter;

    private int cores = 1;

    public GenomeProfiler(String samplesFile, String refFile, String outputFolder) {

        this.samplesFile = samplesFile;
        this.bamFiles = new ArrayList<>();
        this.sampleNames = new ArrayList<>();

        this.refFile = refFile;
        this.outputFolder = outputFolder;

        this.parameterOverview = new ParameterOverview();

        this.windowSummary = WindowSummaryOptions.MEAN;

    }

    public void setWindowSummary(String param) throws Exception {
        switch(param){
            case "mean":
                this.windowSummary = WindowSummaryOptions.MEAN;
                break;
            case "linear":
                this.windowSummary = WindowSummaryOptions.LINEAR;
                break;
            default:
                throw new Exception("ERROR: Invalid window summary option");
        }

    }

    public void run() throws IOException, NoSuchFieldException, IllegalAccessException, InterruptedException {

        if(!new File(outputFolder).exists()) new File(outputFolder).mkdir();

        // init readers
        this.bamReaders = initBamReaders();
        this.refReader = new IndexedFastaSequenceFile(new File(this.refFile));

        // check regions
        SAMSequenceDictionary dict = this.loadGenomeContigs();
        if(regions==null) regions = this.genomeContigs;

        // init micro regions
        List<Region> microRegions = createMicroRegions(windowSize);

        // process batches
        System.out.println("Generating profile (ws = " + this.windowSize + ", t = " + this.cores + ", s = " + this.windowSummary + ")...");
        profileWriter = new PrintWriter(this.outputFolder + "/profile.txt", "UTF-8");
        boolean printHeader = true;

        // print header
        WindowProcessor wpHeader = new WindowProcessor(microRegions.get(0).getChr(),microRegions.get(0).getStart(),microRegions.get(0).getEnd(),this.genomeContigsLength, this.bamReaders, this.sampleNames, this.refReader, this.windowSummary);
        wpHeader.run();
        this.profileWriter.write(wpHeader.getWindow().header() + "\n");

        //
        List<WindowProcessor> wps = new ArrayList<>();
        int count = 0;
        for(Region r :microRegions){

            WindowProcessor wp = new WindowProcessor(r.getChr(),r.getStart(),r.getEnd(),this.genomeContigsLength, this.bamReaders, this.sampleNames, this.refReader, this.windowSummary);
            wps.add(wp);
            count++;

            if(wps.size()==this.cores){
                runThreads(wps);
                wps = new ArrayList<>();
                count = 0;
            }

            printHeader = false;

        }

        if(wps.size()>0){
            runThreads(wps);
        }

        profileWriter.close();

        // histogram construction
        System.out.println("Creating histograms...");
        createHistograms();

        // tabix
        System.out.println("Indexing profile...");
        blockCompressAndIndex(this.outputFolder + "/profile.txt",this.outputFolder + "/profile.txt.gz",false);

        // print parameter stats
        System.out.println("Saving stats...");
        PrintWriter parameterWriter = new PrintWriter(this.outputFolder + "/parameter_stats.txt", "UTF-8");
        parameterWriter.write(parameterOverview.toString() + "\n");
        parameterWriter.close();

    }


    private void runThreads(List<WindowProcessor> wps) throws InterruptedException {

        // prepare threads
        List<Thread> threads = new ArrayList<>();
        for(WindowProcessor wp:wps) {
            Thread wt = new Thread(wp);
            threads.add(wt);
        }

        // launch threads
        for(Thread t:threads) {
            t.setDaemon(true);
            t.start();
        }

        // wait for finishing
        int i=0;
        for(Thread t:threads) {
            t.join();
        }

        // output
        for(WindowProcessor wp:wps) {
            this.profileWriter.write(wp.getWindow().toString() + "\n");
            this.parameterOverview.processWindow(wp.getWindow());
        }

    }

    private void createHistograms() throws IOException {

        // init histograms
        HashMap<String,Histogram> histograms = new HashMap<>();
        for(int i=0; i<parameterOverview.getNumberOfParameters(); i++){
            histograms.put(
                    parameterOverview.getParameterNames().get(i),
                    new Histogram(
                        parameterOverview.getParameterNames().get(i),
                        parameterOverview.getMinValues()[i],
                        parameterOverview.getMaxValues()[i],
                        this.bins
                    )
            );
        }

        // fill histograms
        BufferedReader br = new BufferedReader(new FileReader(this.outputFolder + "/profile.txt"));
        String fields[] = br.readLine().replace("#","").split("\t");
        String line;
        while((line=br.readLine())!=null){
            String values[] = line.split("\t");
            for(int i=3; i<values.length; i++){
                histograms.get(fields[i]).addValue(Double.parseDouble(values[i]));
            }
        }
        br.close();
        for(String key: histograms.keySet()){
            histograms.get(key).computeQuantiles();
        }

        // write histograms to disc
        new File(this.outputFolder + "/histograms").mkdir();
        PrintWriter writer;
        for(String key:histograms.keySet()){
            writer = new PrintWriter(this.outputFolder + "/histograms/" + histograms.get(key).getName() + ".txt", "UTF-8");
            writer.write(histograms.get(key).toString());
            writer.close();
        }

    }


//    /**
//     * Block compress input bed file and create associated tabix index. Newly created file and index are
//     * deleted on exit if deleteOnExit true.
//     * @throws IOException
//     * */
    private void blockCompressAndIndex(String in, String bgzfOut, boolean deleteOnExit) throws IOException {

        File outFile= new File(bgzfOut);

        //LineIterator lin= IOUtils.openURIForLineIterator(inFile.getAbsolutePath());

        BufferedReader lin = new BufferedReader(new FileReader(in));
        BlockCompressedOutputStream writer = new BlockCompressedOutputStream(outFile);
        long filePosition= writer.getFilePointer();

        TabixIndexCreator indexCreator=new TabixIndexCreator(TabixFormat.BED);
        MyBEDCodec bedCodec = new MyBEDCodec();
        String line;
        while((line=lin.readLine())!=null){
            BEDFeature bed = bedCodec.decode(line);
            writer.write(line.getBytes());
            writer.write('\n');
            if(bed!=null) {
                indexCreator.addFeature(bed, filePosition);
            }
            filePosition = writer.getFilePointer();
        }
        writer.flush();

        File tbi= new File(bgzfOut + TabixUtils.STANDARD_INDEX_EXTENSION);
        if(tbi.exists() && tbi.isFile()){
            System.err.println("Warning: Index file already exists: " + tbi);
        }
        Index index = indexCreator.finalizeIndex(writer.getFilePointer());
        index.writeBasedOnFeatureFile(outFile);
        writer.close();

        if(deleteOnExit){
            outFile.deleteOnExit();
            File idx= new File(outFile.getAbsolutePath() + TabixUtils.STANDARD_INDEX_EXTENSION);
            idx.deleteOnExit();
        }
    }


    private SamReader[] initBamReaders() throws IOException {

        loadBamFiles();

        SamReader[] bamReaders = new SamReader[this.bamFiles.size()];
        for(int i=0; i<this.bamFiles.size(); i++) {
            SamInputResource resource = SamInputResource.of(new File(this.bamFiles.get(i))).index(new File(this.bamFiles.get(i) + ".bai"));
            SamReaderFactory factory = SamReaderFactory.makeDefault().enable(SamReaderFactory.Option.CACHE_FILE_BASED_INDEXES).validationStringency(ValidationStringency.LENIENT);
            bamReaders[i] = factory.open(resource);
        }
        return bamReaders;

    }

    private void loadBamFiles() throws IOException {

        BufferedReader bf = new BufferedReader(new FileReader(this.samplesFile));
        String bamFile;
        while((bamFile=bf.readLine())!=null){
            this.bamFiles.add(bamFile);
            this.sampleNames.add(new File(bamFile).getName());
        }

        this.numberOfSamples = this.bamFiles.size();
    }


    public void setRegion(String region){
        regions = new ArrayList<>();
        regions.add(new Region(region));
    }

    public void setRegionFile(String bedFile) throws IOException {
        regions = new ArrayList<>();
        BufferedReader input =  new BufferedReader(new FileReader(bedFile));
        String line;
        String[] fields;
        while (( line = input.readLine()) != null){
            if(!line.startsWith("#")){
                fields = line.split("\t");
                regions.add(new Region(fields[0],Integer.parseInt(fields[1])+1,Integer.parseInt(fields[2])));
            }
        }
    }

    private SAMSequenceDictionary loadGenomeContigs(){
        genomeContigsLength = new HashMap<String,Integer>();
        genomeContigs = new ArrayList<>();
        SAMFileHeader sfh = SamReaderFactory.makeDefault().getFileHeader(new File(this.bamFiles.get(0)));
        SAMSequenceDictionary dict = sfh.getSequenceDictionary();
        for(SAMSequenceRecord s: dict.getSequences()){
            genomeContigsLength.put(s.getSequenceName(),s.getSequenceLength());
            genomeContigs.add(new Region(s.getSequenceName(),1,s.getSequenceLength()));
        }
        return(dict);
    }

    public List<Region> createMicroRegions(int batchSize){

        List<Region> microRegions = new ArrayList<>();
        for(Region r:regions){
            int currentStart = r.getStart();
            int currentEnd;
            while(currentStart<r.getEnd()){
                currentEnd = currentStart + batchSize - 1;
                if(currentEnd>r.getEnd()) currentEnd = r.getEnd();
                microRegions.add(new Region(r.getChr(),currentStart,currentEnd));
                currentStart = currentEnd + 1;
            }
        }

        return microRegions;
    }

    public int getWindowSize() {
        return windowSize;
    }

    public void setWindowSize(int windowSize) {
        this.windowSize = windowSize;
    }

    public int getBins() {
        return bins;
    }

    public void setBins(int bins) {
        this.bins = bins;
    }

    public int getCores() {
        return cores;
    }

    public void setCores(int cores) {
        this.cores = cores;
    }
}
