
import com.sun.tools.javac.util.StringUtils;
import htsjdk.tribble.readers.TabixReader;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;


public class GenomeTester {

    private String targetFile;
    private String profileFolder;
    private String outputFolder;

    private List<Region> regions;
    private List<String> parameterNames;
    private HashMap<String,Histogram> histograms;

    public GenomeTester(String targetFile, String profileFolder, String outputFolder){
        this.targetFile = targetFile;
        this.profileFolder = profileFolder;
        this.outputFolder = outputFolder;
    }

    public void test() throws IOException {

        this.loadTargetRegions();

        this.loadParameterNames();

        if(!new File(outputFolder).exists()) new File(outputFolder).mkdir();

        // query results
        ResultsTable results = new ResultsTable(this.parameterNames);
        TabixReader tr = new TabixReader(this.profileFolder + "/profile.txt.gz");
        String header = tr.readLine();
        String fields[] = header.replace("#","").split("\t");
        fields[0] = "chr.window";
        fields[1] = "start.window";
        fields[2] = "end.window";
        Region r;
        String record;
        for(int i=0; i<regions.size(); i++){
            r = regions.get(i);
            TabixReader.Iterator it = tr.query(r.getChr(),r.getStart(),r.getEnd());
            record = it.next();
            if(record==null){
                results.addRow(r.getChr(),r.getStart(),r.getEnd());
            } else {
                results.addRow(r.getChr(),r.getStart(),r.getEnd(),fields,record.split("\t"));
            }
        }

        PrintWriter pw = new PrintWriter(new File(this.outputFolder + "/query.txt"));
        pw.write(results.toString());
        pw.close();

        // get quantiles
        loadHistograms();
        ResultsTable resultsQuantile = results.createQuantileResults(parameterNames,histograms);

        pw = new PrintWriter(new File(this.outputFolder + "/query_quantile.txt"));
        pw.write(resultsQuantile.toString());
        pw.close();

        pw = new PrintWriter(new File(this.outputFolder + "/ces.txt"));
        pw.write(resultsQuantile.toCES());
        pw.close();

    }

    private void loadTargetRegions() throws IOException {
        regions = new ArrayList<>();
        BufferedReader input =  new BufferedReader(new FileReader(this.targetFile));
        String line;
        String[] fields;
        while (( line = input.readLine()) != null){
            if(!line.startsWith("#")){
                fields = line.split("\t");
                regions.add(new Region(fields[0],Integer.parseInt(fields[1]),Integer.parseInt(fields[2])));
            }
        }
    }

    private void loadParameterNames() throws IOException{

//        BufferedReader br = new BufferedReader(new FileReader(this.profileFolder + "/parameter_stats.txt"));
//        br.readLine();
//        String line;
//        this.parameterNames = new ArrayList<>();
//        while((line=br.readLine())!=null){
//            String values[] = line.split("\t");
//            this.parameterNames.add(values[0]);
//        }
//        br.close();

        this.parameterNames = new ArrayList<>();
        this.parameterNames.add("meanMappingQualitiesErrorProbability");
        this.parameterNames.add("mappingQualityRankSumZ");
        this.parameterNames.add("numberOfCigarElementsRankSumZ");
        this.parameterNames.add("strandBiasSOR");
        this.parameterNames.add("altAlleleFrequency");
        this.parameterNames.add("otherAltAlleleFrequency");
        this.parameterNames.add("indelsFrequency");
        this.parameterNames.add("clippedFrequency");
        //this.parameterNames.add("notProperlyPairedFrequency");
        this.parameterNames.add("mateUnmappedFrequency");
        this.parameterNames.add("nucleotideDiversity");
        this.parameterNames.add("heterozygosity");
        this.parameterNames.add("parsimonyInformativeSite");

    }


    private void loadHistograms() throws IOException {

        // init histograms
        histograms = new HashMap<>();
        for(int i=0; i<this.parameterNames.size(); i++) {
            histograms.put(this.parameterNames.get(i),new Histogram(this.parameterNames.get(i), this.profileFolder + "/histograms/" + this.parameterNames.get(i) + ".txt"));
        }

    }

    public static String repeat(String s, int times) {
        if (times <= 0) return "";
        else return s + repeat(s, times-1);
    }

}
