## Combined Error Score (CES)

### A reference genome assessment from a population scale perspective

Current plant and animal genomic studies are often based on newly assembled genomes that have not been properly consolidated. In this scenario, misassembled regions can easily lead to false positive findings. Despite quality control scores are included within genotyping protocols, they are usually employed to evaluate individual sample quality rather than reference sequence reliability. We propose a statistical model that combines quality control scores across samples in order to detect incongruent patterns at every genomic region. Our model is inherently robust since common artifact signals are expected to be shared between independent samples over misassembled regions of the genome. This tool represents the implementation of this methodology. 

### Tool overview

The reference genome evaluation is performed in two steps
* Build: Empirical distributions of noise estimators are obtained from samples. They are modelled and stored to future uses in that we call Local Genome Profile (LGP).
* Test:  Specific coordinates of interest are evaluated by using a previously built LGP.

The command line interface for CES tools is following described:

<pre><code>
BUILD MODE:

    -build -r ref.fasta -ws 100 -b <sample_1.bam> [sample_2.bam] ... [sample_n.bam] -o <output_folder> [-region chr:start-end] [-bed regions.bed] [-t 2]

TEST MODE

    -test -target design.txt -p profile_folder -o output_folder

PARAMETERS
    -r         Reference genome under evaluation (FASTA format)
    -ws        Window length (in base pairs)
    -b         Mapped samples to perform de evaluation (BAM format)
    -o         Output folder
    -region    Constrain the CES computing to a specific region (chr:start-end)
    -bed       Constrain the CES computing to a specific list of regions (BED format)
    -t         Number of processor to be used
    -target    Regions to test (BED format)
    -p         Profile to used in testing (output folder used in build)  
</code></pre>


### Basic usage

In this example, a new *de novo* assembly needs to be evaluated. To construct the LGP we have prepared 5 samples that were previously mapped against our assembly by using *BWA* mapper. The list of samples is contained in *samples.list* file.

<pre><code>
$ cat samples.list 
sample_1.bam
sample_2.bam
sample_3.bam
sample_4.bam
sample_5.bam
</code></pre>

The first step (LGP construction) is carry out as follows.

<pre><code>
java -jar gopro2.jar -build -r assembly.fasta -ws 500 -b samples.list -o profile
</code></pre>

In this case, we used a 500bp window length, storing the LGP in a folder called "profile".

Time after, we are interested in evaluating the reliability of some regions that contain a set of 10 variants of interest from a previous genotyping study. 

<pre><code>
$ cat loci_of_interest.bed 
#chr	start	end
190	5000	5001
529	300	301
530	10000	10001
533	20000	20001	
534	2700	2701
535	540900	540901
536	40000	40001
540	100000	100001
541	22200	22201
551	96000	96001
</code></pre>

In order to perform the evaluation of these markers we made use of the previously generated profile.

<pre><code>
java -jar gopro2.jar -test -target loci_of_interest.bed -p profile -o test_output
</code></pre>

The result of the evaluation is stored in "test_output" folder. There, CES can be found.

<pre><code>
$ cat test_output/ces.txt
chr	start	end	ces
190	5000	5001    0.001
529	3000	301 0.99
530	10000	10001	0.0003
533	20000	20001	0.001
534	2700	2701	0.78
535	540900	540901	0.1
536	40000	40001	0.004
540	100000	100001	0.134
541	22200	22201	0.89
551	96000	96001	0.35
</code></pre>

Finally, based on CES values, markers from rows 2 (0.99) , 5 (0.78) and 9 (0.89) were filter out of the study since they show strong artifact signals. The rest of markers were ranked and prepared to future analysis.





