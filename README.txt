
CES (Combined Error Score)





||||||||||||||||||||||||||||||

 | CES (combined error score) |
 ||||||||||||||||||||||||||||||

BUILD MODE:

    -build -r <ref.fasta> -b <sample_1.bam> [sample_2.bam] ... [sample_n.bam] -o <output_folder> [-region chr:start-end] [-bed regions.bed] 

TEST MODE

    -test -target <design.txt> -p <profile_folder> -o <output_folder>

PARAMETERS
    -r         Reference genome under evaluation (FASTA format)
    -b         Mapped samples to perform de evaluation (BAM format)
    -o         Output folder
    -region    Constrain the CES computing to a specific region (chr:start-end)
    -bed       Constrain the CES computing to a specific list of regions (BED format)
    -target    Regions to test (BED format)
    -p  